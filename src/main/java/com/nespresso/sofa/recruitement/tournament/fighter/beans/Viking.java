package com.nespresso.sofa.recruitement.tournament.fighter.beans;

public class Viking extends Fighter {

	public Viking() {
		super(null);
		initHitPoints(120, 6);
	}

	public Viking(String equipment) {
		super(equipment);
	}

	public Viking equip(String defenseType) {

		switch (defenseType) {
		case "buckler":
			equipments.add(new Buckler("axe"));
			break;

		default:
			break;
		}

		return this;
	}

}
