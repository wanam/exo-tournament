package com.nespresso.sofa.recruitement.tournament.fighter.beans;

public class Swordsman extends Fighter {

	public Swordsman() {
		super(null);
		initHitPoints(100, 5);
	}

	public Swordsman(String equipment) {
		super(equipment);
	}

	public Swordsman equip(String defenseType) {

		switch (defenseType) {
		case "buckler":
			equipments.add(new Buckler("sword"));
			break;

		case "armor":
			equipments.add(new Armor("sword"));
			break;

		default:
			break;
		}

		return this;
	}

}
