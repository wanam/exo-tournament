package com.nespresso.sofa.recruitement.tournament.fighter.beans;

public class Equipment {

	// used equipment: sword/axe
	private String name;

	public String getName() {
		return name;
	}

	public Equipment(String name) {
		this.name = name;
	}
}
