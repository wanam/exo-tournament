package com.nespresso.sofa.recruitement.tournament.fighter.beans;

public class Buckler extends Equipment {

	private int maxUseTimes;

	// switcher to activate buckler
	private boolean readyToUse = false;

	public Buckler(String name) {
		super(name);
		maxUseTimes = 3;
		readyToUse = false;
	}

	public boolean use() {
		readyToUse = !readyToUse;
		if (readyToUse && (maxUseTimes > 0 || getName().equals("axe"))) {
			maxUseTimes--;
			return true;
		}
		return false;
	}

}
