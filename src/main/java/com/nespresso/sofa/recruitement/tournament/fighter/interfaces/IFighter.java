package com.nespresso.sofa.recruitement.tournament.fighter.interfaces;

public abstract interface IFighter {
	
	public void engage(IFighter fighter);
	public int hitPoints();
	public boolean hasArmor();
	public int getSwordDmg();
	public void initHitPoints(int hitPoints,int swordDmg);

}
