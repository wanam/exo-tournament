package com.nespresso.sofa.recruitement.tournament.fighter.beans;

import java.util.ArrayList;

import com.nespresso.sofa.recruitement.tournament.fighter.interfaces.IFighter;

public class Fighter implements IFighter {

	private int hitPoints;
	private int swordDmg;
	ArrayList<Equipment> equipments = new ArrayList<>();

	public Fighter(String equipment) {
		return;
	}

	@Override
	public void engage(IFighter fighter) {
		if (this.hitPoints > 0) {
			if (this.equipments.size() > 0) {
				for (Equipment equipment : equipments) {
					if (equipment instanceof Buckler) {
						if (((Buckler) equipment).use()) {
							// consume fighter points and return
							fighter.engage(this);
							return;
						}
					} else if (equipment instanceof Armor) {
						int toDeduce = (fighter.hasArmor() ? fighter.getSwordDmg() - 1 : fighter.getSwordDmg());
						this.hitPoints = Math.max(0, this.hitPoints - toDeduce + 3);
						fighter.engage(this);
						return;
					}
				}
			}

			// decrease hit points
			this.hitPoints = Math.max(0, this.hitPoints - fighter.getSwordDmg());

			// consume points from the other part
			fighter.engage(this);
		}
		return;
	}

	@Override
	public boolean hasArmor() {
		for (Equipment equipment : equipments) {
			if (equipment instanceof Armor) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hitPoints() {
		return hitPoints;
	}

	@Override
	public int getSwordDmg() {
		return swordDmg;
	}

	@Override
	public void initHitPoints(int hitPoints, int swordDmg) {
		this.hitPoints = hitPoints;
		this.swordDmg = swordDmg;
	}

}
